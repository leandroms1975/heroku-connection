var altura = 0
var largura = 0
var lifes = 1
var time = 15

var createTimeFly = 1500

var level = window.location.search
level = level.replace('?', '')

if(level === 'normal') {
    createTimeFly = 1500

} else if(level === 'hard') {
    createTimeFly = 900

} else if(level === 'chucknorris') {
    createTimeFly = 750

}

function ajustaTamanhoPalcoJogo(){
    altura = window.innerHeight
    largura = window.innerWidth

    //console.log(altura, largura)
}

ajustaTamanhoPalcoJogo()

var chronometer = setInterval(function() {

    time -= 1

    if(time < 0) {
        clearInterval(chronometer)
        clearInterval(createFly)
        window.location.href = 'victory.html'
    } else {
        document.getElementById('chronometer').innerHTML = time
    }
    
}, 1000)

function randomPosition(){
    
    //remove last fly (if it exists)
    if(document.getElementById('fly')) {
        document.getElementById('fly').remove()

        if(lifes > 3) {
            window.location.href = 'game_over.html'

        } else {
            document.getElementById('h' + lifes).src = "images/empty_heart.png"

            lifes++
        }
    }
    
    var positionX = Math.floor(Math.random() * largura) - 90
    var positionY = Math.floor(Math.random() * altura) - 90

    positionX = positionX < 0 ? 0 : positionX
    positionY = positionY < 0 ? 0 : positionY
    
    console.log(positionX, positionY)
    
    //create html element
    var fly = document.createElement('img')
    fly.src = 'images/fly.png'
    fly.className = randomSize() + ' ' + randomSide()
    fly.style.left = positionX + 'px'
    fly.style.top = positionY + 'px'
    fly.style.position = 'absolute'
    fly.id = 'fly'
    fly.onclick = function() {
        this.remove()
    }
    
    document.body.appendChild(fly)
}

function randomSize(){
    var size = Math.floor(Math.random() * 3)

    switch(size){
        case 0:
            return 'fly1'

        case 1:
            return 'fly2'

        case 2:
            return 'fly3'

    }
}

function randomSide(){
    var side = Math.floor(Math.random() * 2)
    
    switch(side){
        case 0:
            return 'sideR'
    
        case 1:
            return 'sideL'
    
    }
}